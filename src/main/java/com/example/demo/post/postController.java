package com.example.demo.post;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.http.HttpHeaders;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/postArticle")
public class postController {

    @Autowired
    postService PAService;

    @Autowired
    PostRepository repository;

    @GetMapping("/PopularArticle")
    public List<Post> getPopularArticle() {
        return PAService.postPopularArticleService();
    }

    @GetMapping("/ArticleIntoCSV")
    public String ArticleIntoCSV(@RequestBody Post post) {
        return PAService.ArticleIntoCSV(post);
    }

    @PostMapping("/FindArticleByCategory")
    public List<Post> FindArticleByCategory(@RequestBody Post post) {
        return PAService.postFindArticleByCategoryService(post);
    }

    @PostMapping("/FindArticleDetail")
    public Post FindArticleDetail(@RequestBody Post post) {
        return PAService.postFindArticleDetail(post);
    }

    @PostMapping("/SearchArticle")
    public List<Post> SearchArticle(@RequestBody Post post) {
        return PAService.postSearchArticle(post);
    }

    @PostMapping("/CreateArticle")
    public Post CreateArticle(@RequestBody Post post) {
        return PAService.CreateArticle(post);
    }

    @PostMapping("/InsertArticle")
    public Post InsertArticle(@RequestBody Post post) {
        return PAService.InsertArticle(post);
    }

    @PutMapping("/UpdateSubscribe")
    public Post updateSubscribe(@RequestParam String id, @RequestBody Post post) {
        return PAService.updateSubscribe(id, post);
    }

    @PostMapping("/InsertSubscription")
    public Subscription InsertSubscription(@RequestBody Subscription subs) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.telegram.org/bot6253531554:AAE2eILvLq8nKUWvyxZbl0XN3FYhOnwJmI4/sendMessage?chat_id=-891236874&text=Hello&parse_mode=HTML";
        String answer = restTemplate.getForObject(url, String.class);
        return PAService.InsertSubscription(subs);
    }

    @GetMapping("/export")
    public void exportCsv(HttpServletResponse response, @RequestBody Post post) throws IOException {
        // Set response headers
        PAService.export();
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=data.csv");

        // Load data from database or other source
        List<Post> data = repository.findByMonthAndYear(post.getMonth(), post.getYear());

        // Write data to CSV file
        try (PrintWriter writer = response.getWriter()) {
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            csvPrinter.printRecord("id", "url_slug", "title", "view_count");
            for (Post record : data) {
                csvPrinter.printRecord(record.getId(), record.getUrl_slug(), record.getTitle(), record.getViewCount());
            }
            csvPrinter.flush();
        }

    }

}
